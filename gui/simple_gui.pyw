from tkinter import *
class Application(Frame):
    def __init__(self, master):
        super(Application, self).__init__(master)
        self.grid()
        self.bttn_clicks = 0
        self.create_widgets()
    def create_widgets(self):
        self.bttn=Button(self, text = "Clicks quantity: 0")
        self.bttn['command'] = self.update_count
        self.bttn.grid()
    def update_count(self):
        self.bttn_clicks += 1
        self.bttn['text']="Clicks quantity: " + str(self.bttn_clicks)
root = Tk()
root.title("Clicks quantity")
root.geometry("500x250")
app = Application(root)
root.mainloop()


#root = Tk()
#root.title("Простейший GUI")
#root.geometry("200x100")
#app = Frame(root)
#app.grid()
#bttn1 = Button(app, text="Я ничего не делаю!")
#bttn1.grid()
#bttn2=Button(app, text="second")
#bttn2.grid()
#root.mainloop()
              
