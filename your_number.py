import random
def ask_yes_or_no(question):
    """
Задаёт вопрос с ответом 'да' или 'нет'.
    """
    response = None
    while response not in ("larger", "less", "this"):
        response = input(question).lower()
    return response

print (
    """

     This program will try to guess the number you want from 1 to 100

    """
)

word = ''
first = 0
second = 100
while word != 'this':
    third = random.randint(first, second)
    print(third)
    print()
    word = ask_yes_or_no('choice larger or less or this: ')
    if third >= 0 and third <= 100:
        if word == 'less':
            second = third-1
        elif word == 'larger':
            first = third+1
    

input('\Press Enter, for close this programm')
